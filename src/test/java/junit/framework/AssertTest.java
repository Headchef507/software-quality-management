package junit.framework;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class AssertTest {

	@Test
	void testAssertionFail() {
		assertThrows(AssertionFailedError.class, () -> {
			Assert.fail("temp");
		});
	}

	@Test
	void testAssertionFalse() {
		Assert.assertFalse("temp", false);
	}

	@Test
	void testAssertionEquals() {
		Assert.assertEquals("success","check", "check");
	}

	@Test
	void tset_basicAsserTrue(){
		Assert.assertTrue(true);
	}

	@Test
	void tset_basicAsserFalse(){
		Assert.assertFalse(false);
	}
}